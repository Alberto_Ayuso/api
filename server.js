var express = require('express')
var bodyParser = require('body-parser')
var app = express()

var requestJson = require('request-json') //para instanciar la request-json

app.use(bodyParser.json()) //Para leer los bodys se indica que utilice json

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancoaay/collections"
var apiKey = "apiKey=fyLcbQHxbqKAwAs2HP-C2fC_5rbe1fAM" //se hace por motivos de seguridad para llamar a Mlab
var clienteMlab = null

//Inicio de consulta de tabla de usuarios en Mlab
app.get('/apitechu/v5/usuarios', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      var totalusuarios = body.length
      res.send(body)
    }
  })
})
//Fin de deconsulta de tabla de usuarios en Mlab

//Inicio de consultar el nombre y apellido de un usario en Mlab
app.get('/apitechu/v5/usuarios/:id', function(req, res) { //:id es un parámetro en postman sustituyes el :id por el valor
  var id = req.params.id
  var query = 'q={"id": ' + id + '}'
  var filtro = "&f={'first_name': 1, 'last_name': 1, '_id': 0}" //para que sólo muestre el nombre y el apellido
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + filtro + "&l=1&" + apiKey) //"&l=1&" limita que sólo devuelva un registro
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
        res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
//Fin de consultar el nombre y apellido de un usario en Mlab

//Inicio de configuración de operación login en Mlab
app.post('/apitechu/v5/login', function(req, res) {
   var email = req.headers.email
   var password = req.headers.password
   var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1) { //login ok
         clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
         var cambio = '{"$set":{"logged":true}}'
         //JSON.parse(cambio) pone en formato json la variable cambio
         clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
           res.send({"login":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name, "email":body[0].email, "gender":body[0].gender, "country":body[0].country})
         })
       }
       else {
         res.status(404).send('Usuario no encontrado')
       }
     }
   })
})
//Fin de configuración de operación login en Mlab

//Inicio de configuración de operación logout en Mlab
app.post('/apitechu/v5/logout', function(req, res) {
   var id = req.headers.id

   var query = 'q={"id":' + id + ', "logged": true}'
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1) { //estaba logado
         clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
         var cambio = '{"$set":{"logged":false}}'
         //JSON.parse(cambio) pone en formato json la variable cambio
         clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
           res.send({"logout":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name})
         })
       }
       else {
         res.status(200).send('Usuario no logado previamente')
       }
     }
   })
})
//Fin de configuración de operación logout en Mlab

//Inicio de obtener los movimientos de un número de cuenta en Mlab
app.get('/apitechu/v5/movimientocuenta', function(req, res) {
  var iban = req.headers.iban
  var encontrado = false

  var query = 'q={"iban":"' + iban + '"}'
  var filtro = 'f={"movimientos":1,"saldo":1,"_id":0}'

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filtro + "&" + apiKey)
  console.log(clienteMlab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
        res.send(body)
      else {
        res.status(404).send('Cuenta no encontrada')
      }
    }
  })
})
//Fin de obtener los movimientos de un número de cuenta en Mlab

//Inicio de obtener las cuentas que tiene el usuario en Mlab
app.get('/apitechu/v5/cuentas', function(req, res) {
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente + '}'
  var filtro = 'f={"iban":1,"saldo":1,"_id":0}'

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filtro + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
        res.send(body)
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
//Fin de obtener las cuentas que tiene el usuario en Mlab

//Inicio de insertar un movimiento en un número de cuenta en mLab
app.post('/apitechu/v5/movimientocuenta/insertar', function(req, res) {
  var iban = req.headers.iban //iban de la tabla cuenta de mLab
  var movimientonuevo =  {}

  var query = 'q={"iban":"' + iban + '"}'

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey)
  console.log(clienteMlab)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      console.log("Encontrado")
      //console.log(body)
      var numeroinicial = body[0].movimientos.length
      console.log(numeroinicial)

      movimientonuevo.id = numeroinicial + 1
      movimientonuevo.fecha = req.headers.fecha
      movimientonuevo.importe = req.headers.importe
      movimientonuevo.moneda = "EUR"
      movimientonuevo.tipo = "TRANSFERENCIA REALIZADA"
      movimientonuevo.ciudad = req.headers.ciudad
      movimientonuevo.pais = req.headers.pais
      console.log("movimientonuevo:", movimientonuevo)

      objeto_movimientos = body[0].movimientos ////array de los movimientos actuales
      console.log("objeto_movimientos 1:", objeto_movimientos)

      objeto_movimientos[numeroinicial] = movimientonuevo //se añade al array de movimientos el nuevo movimiento
      console.log("objeto_movimientos 2:", objeto_movimientos)

      importenuevo = Number(movimientonuevo.importe)
      nuevo_saldo = body[0].saldo + importenuevo

      //Al usar el operador "$set", solo se actualizan los campos especificados
      //JSON.stringify() Convierte un objeto JavaScript en un valor tipo string
      var cambio = '{"$set":{"movimientos":' + JSON.stringify(objeto_movimientos) + '}}'
      console.log("cambio movimientos:", cambio)

      //Actualiza movimiento en la tabla
      clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
        if (!errP){
          console.log("bodyP de movimientos: ", bodyP)
          var cambio = '{"$set":{"saldo":' + nuevo_saldo + '}}'
          console.log("cambio saldo:", cambio)
          //Actualiza saldo en la tabla
          clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
            if (!errP){
                res.send({"operacion":"Actualizacion saldo","Resultado":"OK"})
            }else {
                res.send({"operacion":"Actualizacion saldo","Resultado":"KO"})
            }
          })
        }else{
          res.send({"operacion":"Realizar transferencia","Resultado":"KO"})
        }
      })
    }
    else {
      res.send({"operacion":"Realizar transferencia","Resultado":"KO"})
    }
  })
})
//Fin de insertar un movimiento en un número de cuenta en mLab

//Inicio de insertar un nuevo usuario en la tabla usuarios de mLab
app.post('/apitechu/v5/altausuarios', function(req, res) {
  var first_name = req.headers.first_name
  var last_name = req.headers.last_name
  var email = req.headers.email
  var gender = req.headers.gender
  var country = req.headers.country
  var password = req.headers.password

  var nuevo_usuario = {}

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get ('', function(err, resM, body) {
    if (!err) {
      var totalusuarios = body.length
      console.log("totalusuarios: ", totalusuarios)

      var query= 'q={ "email": "' + email + '" }' //para buscar si existe el usuario que se quiere dar de alta

      console.log("-" + urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey + "-");

      var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

      clienteMlab.get ('', function(err, resM, body) {

        console.log("body tras select: ",body)
        console.log("body.length: ", body.length)

        if (!err) {
          //Comprueba si devuelve información
          //para ello se pregunta si body.length, una vez que se ha filtrado por email, es igual a 1
          //En caso cierto, devuelve error y fianliza
          //En caso falso, realiza la operación
          if (body.length==1) {
              res.status(409).send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Email ya existente"})
          }
          else {
            console.log("Paso1")
            nuevo_usuario.id = totalusuarios + 1
            nuevo_usuario.first_name = first_name
            nuevo_usuario.last_name = last_name
            nuevo_usuario.email = email
            nuevo_usuario.gender = gender
            nuevo_usuario.ip_address = "192.192.192.192"
            nuevo_usuario.country = country
            nuevo_usuario.password = password
            nuevo_usuario.logged = true

            console.log("Paso2")
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
            console.log("nuevo_usuario: ", nuevo_usuario)
            var cambio = JSON.stringify(nuevo_usuario)

            console.log("cambio: ", cambio)
            console.log("clienteMlab: ", clienteMlab)
            clienteMlab.post('', JSON.parse(cambio),function(errP, resP, bodyP){
              console.log("bodyP: ", bodyP)
              if (!errP) {
                res.send({"Alta de usuario":"OK","Resultado":"Cliente dado de alta de manera correcta","id":bodyP.id,"first_name":bodyP.first_name, "last_name":bodyP.last_name, "email":bodyP.email, "gender":bodyP.gender, "country":bodyP.country})
              }
              else {
                res.status(400).send({"Alta de usuario":"KO", "Resultado":"Error al insertar en la tabla de usuarios" })
              }
            })
          }
        }
        else {
          res.status(400).send({"Operacion":"Alta usuarios","Resultado":"KO", "Detalle":"Error al acceder"})
        }
      })
    }
  })
})
//Fin de insertar un nuevo usuario en la tabla usuarios de mLab

//Inicio de creación del puerto
var port = process.env.PORT || 3000
var fs = require('fs')

app.listen(port)

console.log("API escuchando en el puerto" + port)

var usuarios = require ('./usuarios.json')

console.log("Hola mundo")
//Fin de creación del puerto
