var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');//Carga la aplicación server

var should = chai.should()

chai.use(chaiHttp)//Configurar chai como módulo HTTP
//Inicio de definir un conjunto de pruebas
describe('Tests de conectividad', () => {
  //Probar que google funciona
 it('Google funciona', (done) => {
   chai.request('http://www.google.es')//es como escribir en el ordenador la direccion sin dar enter
       .get('/')//se realiza la peticion en firme
       .end((err, res) => {//función que nos devuelve el error y la respuesta
         //console.log(res)
         res.should.have.status(200)
         done()
       })
 })
})
